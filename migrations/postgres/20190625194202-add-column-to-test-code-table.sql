
-- +migrate Up
create table code_table (
    id serial PRIMARY KEY,
    code text NOT NULL,
    lang text NOT NULL,
    created_at timestamp NOT NULL default current_timestamp,
    updated_at timestamp NOT NULL default current_timestamp
);
create table test_results_table (
    id serial PRIMARY KEY,
    code_table_id text NOT NULL,
    test_table_id text NOT NULL,
    result text,
    judgment boolean NOT NULL,
    created_at timestamp NOT NULL default current_timestamp,
    updated_at timestamp NOT NULL default current_timestamp
);

create table test_table (
    id serial PRIMARY KEY,
    theme  text NOT NULL,
    exam  text NOT NULL,
    created_at timestamp NOT NULL default current_timestamp,
    updated_at timestamp NOT NULL default current_timestamp
);

create table test_cases_table (
    id serial PRIMARY KEY,
    theme  text NOT NULL,
    exam  text NOT NULL,
    created_at timestamp NOT NULL default current_timestamp,
    updated_at timestamp NOT NULL default current_timestamp
);

-- +migrate Down
drop table code_table;
drop table test_results;
drop table test_table;
